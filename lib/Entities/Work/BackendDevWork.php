<?php

namespace Lib\Entities\Work;

class BackendDevWork extends Work
{
    public string $language;
    public function __construct(string $language)
    {
        $this->language = $language;
    }

    public function getWorkName(): string
    {
        return $this->language . ' Backend developer';
    }

}