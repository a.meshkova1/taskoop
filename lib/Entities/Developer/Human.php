<?php

namespace Lib\Entities\Developer;

use Lib\Singleton\DatabaseConnection;

abstract class Human
{
    public function goToWork(string $cry)
    {
        $dbConnection = DatabaseConnection::getInstance();
        $connection = $dbConnection->getConnection();
        echo "пока идешь на роботу можно $cry";
    }

}