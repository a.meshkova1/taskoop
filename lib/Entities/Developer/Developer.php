<?php

namespace Lib\Entities\Developer;

use Lib\Entities\Work\Work;
use Lib\Enums\Grade;

class Developer extends Human implements PrimateInterface
{
    use OfficePlanktonTrait;

    private string $name;
    private Grade $grade;
    private Work $work;

    public function __construct(string $name, Grade $grade)
    {
        $this->name = $name;
        $this->grade = $grade;
    }

    /**
     * метод пользовтеля для похода на работу
     *
     * @link https://docs.google.com/spreadsheets/d/1le8_9GMFpJR1YFLlVgDbmiM5Tv-4ROrfIDXQp5R7kZI/edit#gid=0
     * @version 1.1
     * @todo тело
     * @access public
     * @author та я
     * @param string $cry
     * @return void
     */
    public function goToWork(string $cry): void
    {
        echo "снова воркать";
    }

    public function eat(string $food): void
    {
        echo "я ем $food";
    }

    public function drink(string $drink): void
    {
        echo "я пью $drink";
    }

    public function work(Work $work): void
    {
        $this->work = $work;
        echo "я работаю" . $this->work->getWorkName();
    }

    public function getName(): string
    {

        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getGrade(): string
    {
        return $this->grade->name;
    }

    public function setGrade(Grade $grade): void
    {
        $this->grade = $grade;
    }

}