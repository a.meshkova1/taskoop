<?php

namespace Lib\Entities\Developer;

interface PrimateInterface
{
    public function eat(string $food);
    public function drink(string $drink);

}