<?php

namespace Lib\Singleton;

use PDO;

class DatabaseConnection {
    private static ?DatabaseConnection $instance = null;
    private PDO $connection;

    private function __construct() {
        $this->connection = new PDO('mysql:host=172.31.0.2;dbname=db', 'db', 'db');
    }

    public static function getInstance(): ?DatabaseConnection
    {
        if (self::$instance === null) {
            self::$instance = new DatabaseConnection();
        }
        return self::$instance;
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
