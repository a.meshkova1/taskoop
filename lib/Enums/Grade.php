<?php

namespace Lib\Enums;

enum Grade
{
    case Junior;
    case Middle;
    case Senior;

}
