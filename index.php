<?=

require_once __DIR__ . '/vendor/autoload.php';

use Lib\Entities\Developer\Developer;
use Lib\Entities\Work\BackendDevWork;
use Lib\Entities\Work\FrontendDevWork;
use Lib\Entities\Work\Work;
use Lib\Enums\Grade;
use Lib\Singleton\DatabaseConnection;

$work = new Work();

$dbConnection = DatabaseConnection::getInstance();
$connection = $dbConnection->getConnection();

$frontendWork = new FrontendDevWork();
$backendWork = new BackendDevWork('PHP');

$developerBack = new Developer('Alina', Grade::Junior);
$developerBack->work($backendWork);
try {

    $developerBack->drink(['coffee']);
} catch (TypeError $error) {
    echo 'Ошибка: ', $error->getMessage(), "\n";
}

$developerBack->work($backendWork);
$developerBack->drinkCoffee();

$developerFront = new  Developer('Max', Grade::Senior);
$developerFront->work($frontendWork);

$developerFront->setGrade(Grade::Middle);
echo $developerFront->getGrade();
